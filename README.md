# Analyse et traitement d images

Ce repo est le rendu du travail attendu dans le cadre du projet de classification d'images en analyse et traitement d'images.
Il est découpé en deux parties : la première est l'implémentation de la méthode dite old-school de classification. La deuxième met en place une classification basé sur un réseau neuronal convolutif.
Le rapport à la racine du repo explique en détails le but du projet et détaille son implémentation. Il a été réalisé dans le but d'un projet de classification pour le cours d'analyse et de traitement d'image à l'enssat. 